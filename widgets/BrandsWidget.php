<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;

/**
 * Виджет brands
 * @author Andrey Tregubov <evilkoteg@yandex.ru>
 */
class BrandsWidget extends Widget {
    
    public function init() {
    	parent::init();
    }
    
    public function run() {
    	return $this->render('_brands');
    }
    
}